import PyPDF2
import sys
from pathlib import Path
import settings

import os
from docx2pdf import convert

global tmps


def get_temp_file(name: str) -> str:
    tmp_folder = os.path.join(get_save_directory(), "tmp")
    os.makedirs(tmp_folder, exist_ok=True)
    tmp_folder = os.path.join(tmp_folder, name)
    tmps.append(tmp_folder)
    print("Creating tmp file for convertion, path: " + str(tmp_folder))
    return str(tmp_folder)


def parse_document_index(name: str) -> int:
    if len(name) > 2:
        try:
            return int(name[:2])
        except:
            return 0
    return 0


def get_document_index_prefix() -> str:
    save_dir = get_save_directory()
    list_ = [parse_document_index(x) for x in os.listdir(save_dir)]
    if len(list_) == 0:
        return "01"
    else:
        return str(sorted(list_)[-1] + 1).zfill(2)


def get_save_directory() -> str:
    try:
        return settings.SAVE_TO_DIRECTORY
    except:
        return str(Path(sys.argv[0]).parent)


def get_input_paths() -> [str]:
    return [str(Path(x)) for x in sys.argv[1:]]


def get_document_name_in_pdf(input_full_path: str, prefix: str) -> str:
    final_name = prefix + Path(input_full_path).name
    if final_name.endswith(".docx"):
        return final_name[:-5] + ".pdf"
    return final_name


def append_part(to_append: str, prefix: str):
    target_path = os.path.join(get_save_directory(), prefix + settings.AGGREGATION_FILENAME)

    merger = PyPDF2.PdfFileMerger()
    if os.path.exists(target_path):
        merger.append(PyPDF2.PdfFileReader(open(target_path, 'rb')))
    merger.append(PyPDF2.PdfFileReader(open(to_append, 'rb')))

    os.makedirs(get_save_directory(), exist_ok=True)
    merger.write(target_path)


def save_part(input_full_path: str, pdf: PyPDF2.PdfFileWriter, prefix: str) -> str:
    print("Saving part " + prefix)
    path = os.path.join(get_save_directory(), get_document_name_in_pdf(input_full_path=input_full_path, prefix=prefix))
    os.makedirs(get_save_directory(), exist_ok=True)
    out = open(path, 'wb')
    pdf.write(out)
    out.close()
    return path


def convert_docx_to_pdf(input_full_path: str) -> str:
    tmp_file = get_temp_file(get_document_name_in_pdf(input_full_path=input_full_path, prefix=""))
    print("Converting DOCX to PDF..")
    convert(input_full_path, tmp_file)
    return tmp_file


def process(input_full_path: str):
    print("Processing input on path: " + input_full_path)
    input_inner = input_full_path
    if input_full_path.endswith(".docx"):
        input_inner = convert_docx_to_pdf(input_full_path)
    source = PyPDF2.PdfFileReader(open(input_inner, 'rb'))
    num_pages = source.getNumPages()
    print("Document has '" + str(num_pages) + "' pages.")
    width, height = source.getPage(0).mediaBox[2], source.getPage(0).mediaBox[3]

    odd_pdf = PyPDF2.PdfFileWriter()
    even_pdf = PyPDF2.PdfFileWriter()

    if num_pages % 2 == 1:
        print("Creating blank page.")
        even_pdf.addBlankPage(width=width, height=height)

    for rpagenum in range(num_pages - 1, -1, -1):
        current_page = source.getPage(rpagenum)
        if rpagenum % 2 == 1:  # index is shifted..
            even_pdf.addPage(current_page)
        else:
            odd_pdf.addPage(current_page)
    assert odd_pdf.getNumPages() == even_pdf.getNumPages()

    document_index_prefix = get_document_index_prefix()
    even_generated = save_part(input_full_path=input_full_path,
                               pdf=even_pdf,
                               prefix=document_index_prefix + settings.FIRST_DOCUMENT_PREFIX)
    odd_generated = save_part(input_full_path=input_full_path,
                              pdf=odd_pdf,
                              prefix=document_index_prefix + settings.SECOND_DOCUMENT_PREFIX)
    append_part(even_generated, settings.FIRST_DOCUMENT_PREFIX)
    append_part(odd_generated, settings.SECOND_DOCUMENT_PREFIX)


if __name__ == '__main__':
    tmps = []
    try:
        for input_full_path in get_input_paths():
            process(input_full_path)
    finally:
        for tmp in tmps:
            try:
                os.remove(tmp)
            except:
                pass
